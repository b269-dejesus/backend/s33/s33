// s33 activit1


//////////////////////////////////////////////////
// 1

fetch("https://jsonplaceholder.typicode.com/todos/")
	
.then(response => response.json())
.then((json)=> {
	let chores = json.map(item => item.title);
	console.log(chores);
});
////////////////////////////////////////////////////////////

// 1.1


fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	},
	
})

.then((response)=> response.json())
.then((json)=> console.log(`The item "${json.title}" on the list has status of 
	"${json.completed}" script`));




////////////////////////////////////////////////////
// 2

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
	
})

.then((response)=> response.json())
.then((json)=> console.log(json));


/////////////////////////////////////////
// 3.

fetch("https://jsonplaceholder.typicode.com/todos/",{
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
	"completed": "true",
    "title": "imfinding my brain",
    "userID": 3
	})
})

.then((response)=> response.json())
.then((json)=> console.log(json));



///////////////////////////////
// 4.

fetch("https://jsonplaceholder.typicode.com/todos/3",{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updated to do list item",
 		description: "To update this list when i found my brain",
		status: "Pending",
 		dateCompleted: "Pending",
 		userId: 3
	})
})

.then((response)=> response.json())
.then((json)=> console.log(json));


//////////////////////////////
// 5.



 fetch("https://jsonplaceholder.typicode.com/todos/3",{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updated ",
 		description: "My brain has been found",
		status: "completed",
 		dateCompleted: "03/30/2023",
 		userId: 3
	})
})

.then((response)=> response.json())
.then((json)=> console.log(json));